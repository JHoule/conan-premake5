Conan Premake5 Generator
========================

A Premake5 generator for [Conan](https://conan.io) C/C++ Package manager.

It is based on the [sample Premake4 generator](https://github.com/memsharded/conan-premake)
which is documented [here](http://docs.conan.io/en/latest/howtos/dyn_generators.html#dyn-generators).

Installation
------------

To install, run:

```bash
conan create . jhoule/testing
```

Once installed, it will be accessible inside your conanfile.txt file, e.g.:

```conan
[requires]
Hello/0.1@memsharded/testing
Premake5Gen@0.1@jhoule/testing

[generators]
Premake5
```

Testing
-------

To test, use the example project:

```bash
cd example
conan install . -s compiler="Visual Studio" -b missing
premake5 vs2017
```

The `conan` call will prepare dependencies and generate 3 files:

* conanbuildinfo.txt: A file listing various Conan internal variables.
* conaninfo.txt: A file listing Conan settings being used.
* conanpremake5.lua: The Lua file to include inside your premake5.lua file.

The `premake5` call then generates the project files.

To compile, you can open `vs2017/MySolution.sln`, or compile it directly with MSBuild:

```bash
cd 2017
"c:/Program Files (x86)/Microsoft Visual Studio/2017/Community/MSBuild/15.0/Bin/MSBuild.exe" MySolution.sln /p:Platform=64-bit,Configuration=Release
```

Usage
-----

In your `premake5.lua` file, simply add the generated file on top:

```lua
require 'conanpremake5'
```

then call `conan.use('pkg_name')` to use a dependency, e.g.:

```lua
project "MyApplication"
	kind "ConsoleApp"
	language "C++"
	files { "**.h", "**.cpp" }
	conan.use('Hello')
```

Details
-------

The generated Lua code puts everything in a single variable named `conan`, so as to pollute the Premake5 namespace as little as possible. That variable contains both nested variables as well as utility routines. You can refer to the generated `conanpremake5.lua` contents to see every detail.

That Lua object first contains all of the default Conan variables inside `conan.default`. They are provided for the sake of completeness, but are not meant to be used, except in very specific scenarios. It corresponds to the `deps_build_info` variable inside the Python generator code.

Every dependency is then listed inside a `deps` subtable, where each dependency is listed using its name. For example, the `conan.deps.Hello` table corresponds to the Conan variables for the `Hello` dependency. Each dependency contains all of the Conan settings values, e.g., `includedirs`, `libdirs`, `libs`, etc. It also contains absolute path variations, e.g., `include_paths`, `lib_paths`, etc. These correspond to the `dep_cpp_info` content of each dependency iterated in the Python code.

Then, the `conan.settings` subtable lists all of the Conan settings configured at runtime. Note that some setting keys can contain a dot notation, which is kept untouched, e.g. the compiler runtime and version below:

```lua
	settings = {
		arch = "x86_64",
		arch_build = "x86_64",
		build_type = "Release",
		compiler = "Visual Studio",
		["compiler.runtime"] = "MD",
		["compiler.version"] = "15",
		os = "Windows",
		os_build = "Windows",
	},
```

These settings can be used to tweak the `premake5.lua` file in order to honor some environment tweaks, such at the compiler runtime in `example/premake5.lua`:

```lua
	-- Match compiler runtime: static or dynamic.
	if conan.settings.compiler == "Visual Studio" then
		local uses_static_rt = function(rt) return ({ ["MT"] = true, ["MTd"] = true })[rt] end
		if uses_static_rt(conan.settings["compiler.runtime"]) then
			print("Using static runtime")
			flags { "StaticRuntime" } -- Dynamic is the default.
		else
			print("Using dynamic runtime")
		end
    end
```

Finally, we provide a utility routine called `conan.use()` which takes a package name to use, and for which the Premake5 routines `includedirs`, `libdirs`, and `links` will automatically be called.  This routine is meant to make usage of a Conan dependency as easy and straigthforward as possible.

References
----------

* The [latest Conan docs](http://docs.conan.io/en/latest/)
* The [original Premake4 generator](https://github.com/memsharded/conan-premake) by memshared
* The [custom Premake4 generator documentation](http://docs.conan.io/en/latest/howtos/custom_generators.html#premake-generator-example)
* The [official Premake5 homepage](https://premake.github.io/)
* The [Premake5 wiki page](https://github.com/premake/premake-core/wiki)
* The [Conan Premake5 Generator project page](https://bitbucket.org/JHoule/conan-premake5)