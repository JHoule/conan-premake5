from conans.model import Generator
#from conans.paths import BUILD_INFO
from conans import ConanFile

def do_multiline(arr, limit=70):
    """Determines whether or not the array should be printed over multiple lines."""
    n = len(arr or [])
    if n < 2:
        return False
    len_overhead = 4*n - 1 # 2 for quotes, 2 for spacing except first.
    len_chars = sum(len(v) for v in arr)
    return len_chars + len_overhead > limit

def lua_assignment(var, val, indent_level=1, indent="\t",
                   multi_line='auto', trailing_comma=True):
    """Returns an array of lines representing the Lua assignment."""
    indent_base = indent * indent_level
    if multi_line == 'auto':
        multi_line = isinstance(val, list) and do_multiline(val)
    tc = "," if trailing_comma else ""
    lines = []
    if multi_line:
        indent_block = indent_base + indent
        lines.append("{indent_base}{var} = {{".format(indent_base=indent_base, var=var))
        for v in val:
            v_str = '"%s"' % v.replace("\\", "/")
            lines.append("{indent_block}{v},".format(indent_block=indent_block, v=v_str))
        lines.append("{indent_base}}}{tc}".format(indent_base=indent_base, tc=tc))
    else:
        if isinstance(val, list):
            val_str = ", ".join('"%s"' % v.replace("\\", "/") for v in val)
            lines.append("{indent_base}{var} = {{{val_str}}}{tc}".format(indent_base=indent_base,
                                                                         var=var, val_str=val_str,
                                                                         tc=tc))
        elif isinstance(val, str):
            val_str = '"%s"' % val.replace("\\", "/")
            lines.append("{indent_base}{var} = {val_str}{tc}".format(indent_base=indent_base,
                                                                     var=var, val_str=val_str,
                                                                     tc=tc))
        elif val == None:
            lines.append("{indent_base}{var} = nil{tc}".format(indent_base=indent_base,
                                                               var=var, tc=tc))
    return lines

def lua_key(key):
    """Formats a string such that it is a valid lua table key."""
    if key.find(".") < 0:
        return key
    else:
        return '["' + key + '"]'

class Premake5(Generator):
    @property
    def filename(self):
        return "conanpremake5.lua"

    @property
    def content(self):
        methods = """
	-- A utility routine to efficiently add a package named 'pkg_name' to a Premake project.
	-- It calls 'filter{}' to avoid nesting in the last filter, unless 'skip_filter_clear' is set.
	use = function(pkg_name, skip_filter_clear)
		-- Clear filters, unless expressly skipped.
		if not skip_filter_clear then
			filter{} -- Avoids setting the following inside the last scope only.
		end
		-- Actual Premake configuration calls.
		local pkg_vars = conan.deps[pkg_name]
		includedirs{ pkg_vars.include_paths }
		libdirs{ pkg_vars.lib_paths }
		links{ pkg_vars.libs }
	end
"""
        sections = ["#!lua"]
        deps = self.deps_build_info
        sections.append("conan = {")
        sections.append("	default = {")
        sections.extend(lua_assignment("includedirs", deps.includedirs, 2))
        sections.extend(lua_assignment("libdirs", deps.libdirs, 2))
        sections.extend(lua_assignment("resdirs", deps.resdirs, 2))
        sections.extend(lua_assignment("bindirs", deps.bindirs, 2))
        sections.extend(lua_assignment("builddirs", deps.builddirs, 2))
        sections.extend(lua_assignment("libs", deps.libs, 2))
        sections.extend(lua_assignment("defines", deps.defines, 2))
        sections.extend(lua_assignment("cflags", deps.cflags, 2))
        sections.extend(lua_assignment("cppflags", deps.cppflags, 2))
        sections.extend(lua_assignment("sharedlinkflags", deps.sharedlinkflags, 2))
        sections.extend(lua_assignment("exelinkflags", deps.exelinkflags, 2))
        sections.extend(lua_assignment("rootpath", deps.rootpath, 2))
        sections.extend(lua_assignment("include_paths", deps.include_paths, 2))
        sections.extend(lua_assignment("lib_paths", deps.lib_paths, 2))
        sections.extend(lua_assignment("bin_paths", deps.bin_paths, 2))
        sections.extend(lua_assignment("build_paths", deps.build_paths, 2))
        sections.extend(lua_assignment("res_paths", deps.res_paths, 2))
        sections.append("	},")
        if self.deps_build_info.dependencies:
            sections.append("	deps = {")
            for dep_name, dep_cpp_info in self.deps_build_info.dependencies:
                if dep_name == "Premake5Gen":
                    continue
                dep_name = dep_name.replace("-", "_")
                sections.append("		"+dep_name+" = {")
                sections.extend(lua_assignment("includedirs", dep_cpp_info.includedirs, 3))
                sections.extend(lua_assignment("libdirs", dep_cpp_info.libdirs, 3))
                sections.extend(lua_assignment("resdirs", dep_cpp_info.resdirs, 3))
                sections.extend(lua_assignment("bindirs", dep_cpp_info.bindirs, 3))
                sections.extend(lua_assignment("builddirs", dep_cpp_info.builddirs, 3))
                sections.extend(lua_assignment("libs", dep_cpp_info.libs, 3))
                sections.extend(lua_assignment("defines", dep_cpp_info.defines, 3))
                sections.extend(lua_assignment("cflags", dep_cpp_info.cflags, 3))
                sections.extend(lua_assignment("cppflags", dep_cpp_info.cppflags, 3))
                sections.extend(lua_assignment("sharedlinkflags", dep_cpp_info.sharedlinkflags, 3))
                sections.extend(lua_assignment("exelinkflags", dep_cpp_info.exelinkflags, 3))
                sections.extend(lua_assignment("rootpath", dep_cpp_info.rootpath, 3))
                sections.extend(lua_assignment("include_paths", dep_cpp_info.include_paths, 3))
                sections.extend(lua_assignment("lib_paths", dep_cpp_info.lib_paths, 3))
                sections.extend(lua_assignment("bin_paths", dep_cpp_info.bin_paths, 3))
                sections.extend(lua_assignment("build_paths", dep_cpp_info.build_paths, 3))
                sections.extend(lua_assignment("res_paths", dep_cpp_info.res_paths, 3))
                sections.append("		},")
        sections.append("	},")
        sections.append("	settings = {")
        for key,val in self.settings.items():
            sections.extend(lua_assignment(lua_key(key), val, 2))
        sections.append("	},")
        sections.append(methods)
        sections.append("}")
        return "\n".join(sections)


class MyCustomGeneratorPackage(ConanFile):
    name = "Premake5Gen"
    version = "0.1"
    description = "A Premake5 generator for Conan"
    url = "https://github.com/jhoule/conan-premake5"
    license = "public domain"

    def build(self):
        pass

    def package_info(self):
        self.cpp_info.includedirs = []
        self.cpp_info.libdirs = []
        self.cpp_info.bindirs = []
