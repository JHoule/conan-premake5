require 'conanpremake5'

solution "MySolution"
	configurations { "Debug", "Release" }
	platforms { "32-bit", "64-bit" }

	location(_ACTION)

	cppdialect "C++14"

	flags {
		"MultiProcessorCompile",
	}

	-- Match compiler runtime: static or dynamic.
	if conan.settings.compiler == "Visual Studio" then
		local uses_static_rt = function(rt) return ({ ["MT"] = true, ["MTd"] = true })[rt] end
		if uses_static_rt(conan.settings["compiler.runtime"]) then
			print("Using static runtime")
			flags { "StaticRuntime" } -- Dynamic is the default.
		else
			print("Using dynamic runtime")
		end
	end

	characterset ( "MBCS" ) -- Default, Unicode, MBCS.

	filter { "platforms:32-bit" }
		system "Windows"
		architecture "x32"

	filter { "platforms:64-bit" }
		system "Windows"
		architecture "x64"

	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		defines { "NDEBUG" }
		symbols "On"
		optimize "On"

project "MyApplication"
	kind "ConsoleApp"
	language "C++"
	files { "**.h", "**.cpp" }
	conan.use('Hello')
